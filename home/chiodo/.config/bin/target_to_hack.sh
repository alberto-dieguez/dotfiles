#!/bin/bash

ip_address=$(cat /home/chiodo/.config/bin/target/target.txt | awk '{print $1}')
machine_name=$(cat /home/chiodo/.config/bin/target/target.txt | awk '{print $2}')

if [ $ip_address ] && [ $machine_name ]; then
	echo "%{F#00ff00}什 %{F#ffffff}$ip_address%{u-} - $machine_name"
else
	echo "%{F#ff0000}什 %{u-}%{F#ffffff} No target"
fi
