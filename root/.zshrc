# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Fix the Java Problem
export _JAVA_AWT_WM_NONREPARENTING=1

# Prompt
PROMPT="%F{red}┌[%f%F{cyan}%m%f%F{red}]─[%f%F{yellow}%D{%H:%M-%d/%m}%f%F{red}]─[%f%F{magenta}%d%f%F{red}]%f"$'\n'"%F{red}└╼%f%F{green}$USER%f%F{yellow}$%f"

# Export PATH$
export PATH=~/.local/bin:/snap/bin:/usr/sandbox/:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/usr/share/games:/usr/local/sbin:/usr/sbin:/sbin:$PATH

# alias
alias ls='ls -lh --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
#####################################################
# Auto completion / suggestion
# Mixing zsh-autocomplete and zsh-autosuggestions
# Requires: zsh-autocomplete (custom packaging by Parrot Team)
# Jobs: suggest files / foldername / histsory bellow the prompt
# Requires: zsh-autosuggestions (packaging by Debian Team)
# Jobs: Fish-like suggestion for command history
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh-autocomplete/zsh-autocomplete.plugin.zsh
# Select all suggestion instead of top on result only
zstyle ':autocomplete:tab:*' insert-unambiguous yes
zstyle ':autocomplete:tab:*' widget-style menu-select
zstyle ':autocomplete:*' min-input 2
bindkey $key[Up] up-line-or-history
bindkey $key[Down] down-line-or-history
#################################################
# Fish like syntax highlighting
# Requires "zsh-syntax-highlighting" from apt

source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Save type history for completion and easier life
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
#setopt appendhistory
setopt histignorealldups sharehistory

# Useful alias for benchmarking programs
# require install package "time" sudo apt install time
# alias time="/usr/bin/time -f '\t%E real,\t%U user,\t%S sys,\t%K amem,\t%M mmem'"
# Display last command interminal
echo -en "\e]2;Parrot Terminal\a"
preexec () { print -Pn "\e]0;$1 - Parrot Terminal\a" }

source ~/.config/powerlevel10k/powerlevel10k.zsh-theme

# Custom Aliases
alias cat='/bin/batcat'
alias catn='/bin/cat'
alias catnl='/bin/batcat --paging=never'

# Manual aliases
alias ll='lsd -lh --group-dirs=first'
alias la='lsd -a --group-dirs=first'
alias l='lsd --group-dirs=first'
alias lla='lsd -lha --group-dirs=first'
alias ls='lsd --group-dirs=first'

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

POWERLEVEL9K_DISABLE_CONFIGURATION_WIZARD=true

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

source /usr/share/zsh-plugins/sudo.plugin.zsh

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ $KEYMAP == vicmd ]] || [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ $KEYMAP == main ]] || [[ $KEYMAP == viins ]] || [[ $KEYMAP = '' ]] || [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select

# Start with beam shape cursor on zsh startup and after every command.
zle-line-init() { zle-keymap-select 'beam'}

# Extract nmap information
function extractPorts(){
	ports="$(cat $1 | grep -oP '\d{1,5}/open' | awk '{print $1}' FS='/' | xargs | tr ' ' ',')"
	ip_address="$(cat $1 | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}' | sort -u | head -n 1)"
	echo -e "\n[*] Extracting information...\n" > extractPorts.tmp
	echo -e "\t[*] IP Address: $ip_address"  >> extractPorts.tmp
	echo -e "\t[*] Open ports: $ports\n"  >> extractPorts.tmp
	echo $ports | tr -d '\n' | xclip -sel clip
	echo -e "[*] Ports copied to clipboard\n"  >> extractPorts.tmp
	cat extractPorts.tmp; rm extractPorts.tmp
}

# Functions
function mkt(){
	mkdir {nmap,content,exploits,scripts}
}

# Set target
function settarget(){
	ip_address=$1
	machine_name=$2
	echo "$ip_address $machine_name" > /home/chiodo/.config/bin/target/target.txt
}

function setlayout(){
	echo "1.  Español"
	echo "2.  Inglés"
	read layout
	if [[ $layout -eq 1 ]]; then
          setxkbmap es
        elif [ $layout -eq 2 ]; then
          setxkbmap us
        else
          echo "Selección no válida."
        fi
}


############################################### TEST
shift-arrow() {
    ((REGION_ACTIVE)) || zle set-mark-command
    zle $1
}
shift-left() shift-arrow backward-char
shift-right() shift-arrow forward-char
zle -N shift-left
zle -N shift-right
  
bindkey $terminfo[kLFT] shift-left
bindkey $terminfo[kRIT] shift-right

bindkey "^[[1;5D" vi-backward-word
bindkey "^[[1;5C" vi-forward-word
bindkey "^[[1;5D" vi-backward-word
bindkey "^[[1;5C" vi-forward-word
# CTRL + a  = beginning-of-line
# CTRL + e  = end-of-line
# CTRL + w  = Delete all backward
# ESC + d   = Delete from your point to forward
# ESC + a   = Delete all line
# CTRL + A	Move to the beginning of the line
# CTRL + E	Move to the end of the line
# CTRL + [left arrow]	Move one word backward (on some systems this is ALT + B)
# CTRL + [right arrow]	Move one word forward (on some systems this is ALT + F)
# CTRL + U (bash)	Clear the characters on the line before the current cursor position
# CTRL + U (zsh)	If you're using the zsh, this will clear the entire line
# CTRL + K	Clear the characters on the line after the current cursor position
# ESC + [backspace]	Delete the word in front of the cursor
# CTRL + W	Delete the word in front of the cursor
# ALT + D	Delete the word after the cursor
# CTRL + R	Search history
# CTRL + G	Escape from search mode
# CTRL + _	Undo the last change
# CTRL + L	Clear screen
# CTRL + S	Stop output to screen
# CTRL + Q	Re-enable screen output
# CTRL + C	Terminate/kill current foreground process
# CTRL + Z	Suspend/stop current foreground process
